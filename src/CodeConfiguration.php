<?php

/** @noinspection SpellCheckingInspection */

namespace JohnSear\OTP;

use Exception;
use JohnSear\OTP\DependencyInjection\Base32Encoder;

class CodeConfiguration
{
    private const DEFAULT_DIGITS    = 6;
    private const DEFAULT_PERIOD    = 15;
    private const DEFAULT_ALGORITHM = 'SHA1';
    private const DEFAULT_OTP_AUTH  = 'TOTP';
    private const DEFAULT_ISSUER    = 'John_Sear | OTP Library';
    private const DEFAULT_USERNAME  = 'John_Sear@gmx.de';

    /** @var int */
    private $digits = self::DEFAULT_DIGITS;
    /** @var int */
    private $period = self::DEFAULT_PERIOD;
    /** @var string */
    private $algorithm = self::DEFAULT_ALGORITHM;
    /** @var string */
    private $otpAuth = self::DEFAULT_OTP_AUTH;
    /** @var string */
    private $issuer = self::DEFAULT_ISSUER;
    /** @var string */
    private $username = self::DEFAULT_USERNAME;
    /** @var string */
    private $base32Secret;

    /**
     * @param int $digits OTP Code length
     */
    public function setDigits(int $digits): CodeConfiguration
    {
        $this->digits = $digits;
        return $this;
    }

    public function getDigits(): int
    {
        return $this->digits;
    }

    /**
     * @param int $period Period in Seconds while changing code
     */
    public function setPeriod(int $period): CodeConfiguration
    {
        $this->period = $period;
        return $this;
    }

    public function getPeriod(): int
    {
        return $this->period;
    }

    public function setAlgorithm(string $algorithm): CodeConfiguration
    {
        $this->algorithm = $algorithm;
        return $this;
    }

    public function getAlgorithm(): string
    {
        $allowedAlgorithms = [
            'SHA1',
            'SHA256',
            'SHA512',
        ];
        return (in_array(strtoupper($this->algorithm), $allowedAlgorithms, true)) ? strtoupper($this->algorithm) : self::DEFAULT_ALGORITHM;
    }

    public function setOtpAuth(string $otpAuth): CodeConfiguration
    {
        $this->otpAuth = $otpAuth;
        return $this;
    }

    public function getOtpAuth(): string
    {
        $allowedTypes = [
            'TOPT',
            'HOPT',
        ];
        return (in_array(strtoupper($this->otpAuth), $allowedTypes, true)) ? strtoupper($this->otpAuth) : self::DEFAULT_OTP_AUTH;
    }

    public function setIssuer(string $issuer): CodeConfiguration
    {
        $this->issuer = $issuer;
        return $this;
    }

    public function getIssuer(): string
    {
        return $this->issuer;
    }

    public function setUsername(string $username): CodeConfiguration
    {
        $this->username = $username;
        return $this;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function setBase32Secret(string $base32Secret): CodeConfiguration
    {
        $this->base32Secret = $base32Secret;
        return $this;
    }

    public function getBase32Secret(): string
    {
        try {
            /** 20 bytes = 160 bits */
            $random_bytes = random_bytes(20);

            /** Base32 encoded */
            $randomBase32Secret = Base32Encoder::encode($random_bytes);
        } catch (Exception $e) {
            debug($e->getMessage());
            $randomBase32Secret = 'K4N4WL6OKFQXX7BIKNEOIS2LAXGKQJWD';
        }

        return $this->base32Secret = $this->base32Secret ?? $randomBase32Secret;
    }

    public function __toArray():array
    {
        return [
            'digits'       => $this->getDigits(),
            'period'       => $this->getPeriod(),
            'algorithm'    => $this->getAlgorithm(),
            'otp_auth'     => $this->getOtpAuth(),
            'issuer'       => $this->getIssuer(),
            'username'     => $this->getUsername(),
            'base32Secret' => $this->getBase32Secret(),
        ];
    }
}
