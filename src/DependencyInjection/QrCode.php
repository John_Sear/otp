<?php

declare(strict_types=1);

namespace JohnSear\OTP\DependencyInjection;

use Endroid\QrCode\Color\Color;
use Endroid\QrCode\Color\ColorInterface;
use Endroid\QrCode\Encoding\Encoding;
use Endroid\QrCode\ErrorCorrectionLevel\ErrorCorrectionLevelLow;
use Endroid\QrCode\QrCode as EnQrCode;
use Endroid\QrCode\RoundBlockSizeMode\RoundBlockSizeModeMargin;
use Endroid\QrCode\Writer\PngWriter;
use Exception;

class QrCode
{
    /**
     * Generate a data URI to include QR-Code image data inline (i.e. inside an <img> tag)
     *
     * @throws Exception
     */
    public static function generateDataUri(string $data, array $options = []): string
    {
        $size            = $options['size'] ?? 300;
        $margin          = $options['margin'] ?? 10;
        $foregroundColor = (($fgc = ($options['foregroundColor'] ?? null)) instanceof ColorInterface) ? $fgc : new Color(0, 0, 0);
        $backgroundColor = (($bgc = ($options['backgroundColor'] ?? null)) instanceof ColorInterface) ? $bgc : new Color(255, 255, 255);

        $qrCode = EnQrCode::create($data)
            ->setEncoding(new Encoding('UTF-8'))
            ->setErrorCorrectionLevel(new ErrorCorrectionLevelLow())
            ->setSize($size)
            ->setMargin($margin)
            ->setRoundBlockSizeMode(new RoundBlockSizeModeMargin())
            ->setForegroundColor($foregroundColor)
            ->setBackgroundColor($backgroundColor);

        // Generate a data URI to include image data inline (i.e. inside an <img> tag)
        return (new PngWriter())->write($qrCode)->getDataUri();
    }
}
