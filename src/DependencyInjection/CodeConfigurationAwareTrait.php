<?php

namespace JohnSear\OTP\DependencyInjection;

use JohnSear\OTP\CodeConfiguration;

trait CodeConfigurationAwareTrait
{
    /** @var CodeConfiguration */
    protected $codeConfiguration;

    public function setCodeConfiguration(CodeConfiguration $codeConfiguration): self
    {
        $this->codeConfiguration = $codeConfiguration;
        return $this;
    }

    public function getCodeConfiguration(): CodeConfiguration
    {
        return (!$this->codeConfiguration instanceof CodeConfiguration) ? new CodeConfiguration() : $this->codeConfiguration;
    }
}
