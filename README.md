# JohnSear OTP Library
(c) 2023 by [John_Sear](https://bitbucket.org/John_Sear/otp/)

> This Library can be used to Generate and Validate OTP Codes.

## Features
* **Fully Flexible** to use OTP Code Configuration or leave all to defaults
* **Generate** OTP Code and check last creation
* **Validate** OTP Code
* **QrCode Image** Data Uri generation  
  _Example_:  
  ![OTP Configuration QrCode](https://bitbucket.org/John_Sear/otp/raw/master/doc/otp-qrcode.png)

## Documentation
Full Documentation, i.e. installation with Composer, can be found [here](https://bitbucket.org/John_Sear/otp/src/master/doc/index.md)
