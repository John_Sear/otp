# JohnSear JWT Package - Documentation

## Installation
* [With Composer](https://bitbucket.org/John_Sear/otp/src/master/doc/composer-install.md) (vendor Area)

## OTP Code Usage
* [OTP Code - Configure](https://bitbucket.org/John_Sear/otp/src/master/doc/otp-configuration.md)
* [OTP Code - Generate and Validate](https://bitbucket.org/John_Sear/otp/src/master/doc/otp-usage.md)
* [OTP Code - QrCode](https://bitbucket.org/John_Sear/otp/src/master/doc/otp-qrcode.md)
