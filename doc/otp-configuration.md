# OTP Code Configuration

The _DataTransferObject_ `JohnSear\OTP\CodeConfiguration` can be used to set **digits**, **period**, **algorithm** for encrypting, the **otpAuth** type and **base32Secret**, a base32 encoded secret.
The additional configuration parameter for QrCode generation can be set with **issuer** and **username**.

> This is completely optional, every configuration parameter has a default value

## OTP Generation

| Name           | Type    | Default                                 | Description                                                                                                                                     |
|:---------------|:--------|:----------------------------------------|:------------------------------------------------------------------------------------------------------------------------------------------------|
| `digits`       | integer | 6                                       | Usually `6` or `8` is used                                                                                                                      |
| `period`       | integer | 15                                      | The interval in seconds i.e. `15`, `30`, `60`, `120`, `300` ...                                                                                 |
| `algorithm`    | string  | 'SHA1'                                  | Used for Encrypting. Allowed algorithms are `'SHA1'`, `'SHA256'` and `'SHA512'` for now.                                                        |
| `otpAuth`      | string  | 'TOPT'                                  | The OTP Code type. Allowed ones are `'TOTP'` (_Time-based One-Time Password Algorithm_) and `'HOTP'` (_HMAC-based One-Time Password Algorithm_) |
| `base32Secret` | string  | _random 20 Bytes Base32 encoded string_ | The Secret, Base32 encoded.                                                                                                                     |
| `issuer`       | string  | 'John_Sear \| OTP Library'              | I.e. Business Name for the Label value                                                                                                          |
| `username`     | string  | 'John_Sear@gmx.de'                      | I.e. User Name / E-Mail Address for the Label value                                                                                             |

