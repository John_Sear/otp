# OTP QrCode DataUri
Most OTP Apps can be configured via scanning a QrCode. This Library provides the generation of a data URI to include QR-Code image data inline inside an `<img />` tag

## Endroid\QrCode
To generate the QrCode this OTP Library is using the [Endroid QrCode Library](https://github.com/endroid/qr-code)

## OTP Code Configuration 
It is recommendet, but not needed to set up the [OTP Code Configuration](https://bitbucket.org/John_Sear/otp/src/master/doc/otp-configuration.md) DataTransferObject.

> If you use the default configuration, be sure to use **your own Base32 encoded Secret** in every usage, for working results!

## Generate QrCode DataUri

### QrCode Options
Image Options can be optionally added to generate the QrCode as an associated Array.

```php
use Endroid\QrCode\Color\Color;

require __DIR__ . '/vendor/autoload.php';

$OTPCodeGenerator = new OTP\CodeGenerator();

$options = [
    'size'            => 300,
    'margin'          => 10,
    'foregroundColor' => new Color(0, 0, 0),
    'backgroundColor' => new Color(255, 255, 255),
];

```

| Name              | Type                                  | Default                                            | Description                                 |
|:------------------|:--------------------------------------|:---------------------------------------------------|:--------------------------------------------|
| `size`            | integer                               | 300                                                | Length and Width Value for the QrCode Image |
| `margin`          | integer                               | 10                                                 | Buffer Border around squares                |
| `foregroundColor` | `Endroid\QrCode\Color\ColorInterface` | new&nbsp;Endroid\QrCode\Color\Color(0, 0, 0)       | Color Object for the Foreground color       |
| `backgroundColor` | `Endroid\QrCode\Color\ColorInterface` | new&nbsp;Endroid\QrCode\Color\Color(255, 255, 255) | Color Object for the Background color       |

> Be aware that the width and length of the QrCode Image will be `size + (2 x margin)`. I.e. the width and length with the default values will be `320px`! 

### Example including OTP Code Configuration
```php
use JohnSear\OTP;

require __DIR__ . '/vendor/autoload.php';

$OTPCodeGenerator = new OTP\CodeGenerator();
$OTPCodeValidator = new OTP\CodeValidator($OTPCodeGenerator);
$OTPCodeProvider  = new OTP\CodeProvider($OTPCodeGenerator, $OTPCodeValidator);

$myOTPCodeConfiguration = (new OTP\CodeConfiguration())
    ->setDigits(6)
    ->setPeriod(15)
    ->setAlgorithm('SHA1')
    ->setOtpAuth('TOTP')
    ->setBase32Secret('BASE32ENCODEDSECRET')
    ->setIssuer('John_Sear | OTP Library')
    ->setUsername('John_Sear@gmx.de');

$OTPCodeProvider->setCodeConfiguration($myOTPCodeConfiguration);

$options = [
    'size'            => 300,
    'margin'          => 10,
    'foregroundColor' => new Color(0, 0, 0),
    'backgroundColor' => new Color(255, 255, 255),
];

// Generate DataUri
$QrCodeDataUri = $OTPCodeProvider->generateQrCode($options);
// Result: data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAUAAAAFACAIAAABC8jL9AAAACXBIWXMAAA7EAAAOxAGVKw4bAAAJGElEQVR4nO3dUW7kOAwEUGcx979y9n/d2BEEklIl732PLbc7BQFsDvX1/f39AJn+Of0AwD4BhmACDMEEGIIJMAQTYAgmwBBMgCGYAEMwAYZgAgzBBBiC/Vn8d19fX63P8f/e/+Pi/Twr/yuj6qq3lSdcuWpF1Z1X3sbZ93z2qtv+5j+yA0MwAYZgAgzBBBiCCTAEW61Cv/XN4jlb/auq8fZVNVfuvFJl3as5T9bk+67ac+HfvB0YggkwBBNgCCbAEEyAIdh+Ffptr5I2WdnbW6uqt7bqk/Z1/+5V16skdjUf/5u3A0MwAYZgAgzBBBiCCTAEq6xC32ay5rzyb6pqj1Wfa7Jn+LZTbG97nm12YAgmwBBMgCGYAEMwAYZgqVXoyb7iqmptVV16cmJJVU2+z9n+7ePswBBMgCGYAEMwAYZgAgzBKqvQk7W+qonBZ/uKJ2eGrKw+eVXVVOq+b3nF8fq2HRiCCTAEE2AIJsAQTIAh2H4V+mwH7NttFcuzz7N3n77zCm9TNYP6ODswBBNgCCbAEEyAIZgAQ7DVKvTxns//OFubXfk3Z+u3k9/X2fMKb5u/PcwODMEEGIIJMAQTYAgmwBBstQq9NwFj7z5Vd666qq+vuMpkj+793dGT77nvsy/WwO3AEEyAIZgAQzABhmACDMG+Fotdw7W1v66+V/07WydfMTnTuK+mOvnMe/p+oVhZq+pv9bEDQzQBhmACDMEEGIIJMARbrUJ/uPL6qu9tMzHur+SvXLXyPGcnYL/ddudCdmAIJsAQTIAhmABDMAGGYJVV6Naez4MunMOwYbLLemX1FRf2Hv9VX93+IzswBBNgCCbAEEyAIZgAQ7DKudBvfd22e/bWmuz1PVvbn6wVVzl7LmRfh/kiOzAEE2AIJsAQTIAhmABDsPNzoRPnZkzWePtW76ucv03+jlDl7OQTvdDw8wkwBBNgCCbAEEyAIdhqL/SksxOnq9bq66nuM7l6X1/6ituq69v3sQNDMAGGYAIMwQQYggkwBNufC7253vWdxm+Tlc+3qh7myVkfidObQ89qtANDMAGGYAIMwQQYggkwBOvthe6rRvbd563vee7/7HtV39sq1VUnM05O9l5kB4ZgAgzBBBiCCTAEE2AIVnk64dnpBGfnb+z9myp7lc+zZyz2TUvee/OTv5is3HmRHRiCCTAEE2AIJsAQTIAh2H4vdFWvb1V9++yEhxV9a/V19r5V9RVP3qdv0sjZOd6PHRiiCTAEE2AIJsAQTIAh2GoV+mwduG+iQt8ZeZO9vlW10Kr3vOLsWY19NfC9tcyFht9IgCGYAEMwAYZgAgzBVk8nPFtlrdI3r3hF35mGk6cKrtx5ssa78jx7a93fm/3YgSGaAEMwAYZgAgzBBBiCrVahP1w5WJdeuc/b/TMWJt/Y3p1v69a+7Tut6ug2Fxp+IwGGYAIMwQQYggkwBKucC73n7KmCx6uIJasn6jsFcvKN7f0lmMgBPI8AQzQBhmACDMEEGIJV9kLvub8Xus/PeIdVd/4Z8zeqqELDzyfAEEyAIZgAQzABhmCrvdC3nea2p+rMvhVV04DvP2fwra/CPPkN9vU5F7IDQzABhmACDMEEGIIJMATbn8jR10l7tl5addXepIiz3dF9lfO+ORV9tf3J0wnNhYbfSIAhmABDMAGGYAIMwSrnQk/2qe7duW8W8dtkP3DVO+yrzVat/lb1fs7W/7fZgSGYAEMwAYZgAgzBBBiCrc6F7pvD3HefFX1dzVXPMzlbY3Iy88rqZ7+dsydOmgsNP58AQzABhmACDMEEGILtV6H7as59VbvJMw3P1q5XDM+OKFl9xdlfMfqmiHxkB4ZgAgzBBBiCCTAEE2AItlqF3rz7YBXxtkr13p331rrtmffc9lb7mAsNPI8AQzQBhmACDMEEGIKtzoWerKBWzZfuOzVvb/W+juVJZ09mrDrl8K1vHshb4TdoB4ZgAgzBBBiCCTAEE2AItt8LPTlR4eypeW+T/beTXd9n3/xt9fa3qic0kQN4HgGGaAIMwQQYggkwBKs8nTBRVd31tmkbZ2vXKxLPl5x8P4vswBBMgCGYAEMwAYZgAgzBVidyTNY532476a9qrbM93n3vsG8+ycpaVVf1vXm90MDzCDBEE2AIJsAQTIAh2GoVes/kjI7J6vGeqinZ98+FXlH1K8bZ7/34t2MHhmACDMEEGIIJMAQTYAi2P5HjthnCe2udncOQOFu7r+7aN3ukaq09rVPE7cAQTIAhmABDMAGGYAIMwXp7od8m665V+jpybzuHcXLSyNttU5erPkXf5JPHDgzRBBiCCTAEE2AIJsAQbLUX+sOV19cDV5yd59Ban9xY/W1y+nfV6iv65pwPn/loB4ZgAgzBBBiCCTAEE2AINt0L/dbX/bty574pEGc7n89OwF7Rd5pk1ffet1bhWY12YAgmwBBMgCGYAEMwAYZglb3QK/pqqrfdeWWtyW7kyR7dyVnNZ8+pPP7rgx0YggkwBBNgCCbAEEyAIdj+6YQrqirDfVMg+qqRK2v11TD3nD1RceV57u/xnpxY8tiBIZoAQzABhmACDMEEGIKtTuTom/k8udZtcxhW3DaxpK+z932fyc9edZ/JeemPHRiiCTAEE2AIJsAQTIAh2GoV+mwP6t4c3apZDX3V2pXnOVsDP+vs2ZER79AODMEEGIIJMAQTYAgmwBBs/3TCsx3LfRXCqikZt32KFVV18pU77z3P3uqTdx5e3Q4MwQQYggkwBBNgCCbAEGy/Cv02OTt6z1717+xpgCvuP2uvb63ECeGF867twBBMgCGYAEMwAYZgAgzBKqvQk/YqjVU9zMOVxo3VJ9fau+psJb/vPsPnS9qBIZgAQzABhmACDMEEGIKlVqGrnK189tU5qyqfe2utmHzClatWnP1l4SM7MAQTYAgmwBBMgCGYAEOwyir0/bM1qmqqfauvuH+yx+Sd91bvO6eyqk/eXGj4+QQYggkwBBNgCCbAEGy/Cn22rlilai7E/W/jts+1t9bkHI+qJ2xlB4ZgAgzBBBiCCTAEE2AI9jXZwAzUsgNDMAGGYAIMwQQYggkwBBNgCCbAEEyAIZgAQzABhmACDMEEGIL9CxSiob0a/XIGAAAAAElFTkSuQmCC

// Image Output
echo '<img src="' . $QrCodeDataUri . '" alt="OTP Configuration QrCode" />';
```

### Example only with own Base32 encoded secret
```php
use JohnSear\OTP;

require __DIR__ . '/vendor/autoload.php';

$OTPCodeGenerator = new OTP\CodeGenerator();
$OTPCodeValidator = new OTP\CodeValidator($OTPCodeGenerator);
$OTPCodeProvider  = new OTP\CodeProvider($OTPCodeGenerator, $OTPCodeValidator);

$ownBase32Secret = 'BASE32ENCODEDSECRET';

$options = [
    'size'            => 300,
    'margin'          => 10,
    'foregroundColor' => new Color(0, 0, 0),
    'backgroundColor' => new Color(255, 255, 255),
];

// Generate DataUri
$QrCodeDataUri = $OTPCodeProvider->generateQrCode($options, $ownBase32Secret);
// Result: data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAUAAAAFACAIAAABC8jL9AAAACXBIWXMAAA7EAAAOxAGVKw4bAAAJGElEQVR4nO3dUW7kOAwEUGcx979y9n/d2BEEklIl732PLbc7BQFsDvX1/f39AJn+Of0AwD4BhmACDMEEGIIJMAQTYAgmwBBMgCGYAEMwAYZgAgzBBBiC/Vn8d19fX63P8f/e/+Pi/Twr/yuj6qq3lSdcuWpF1Z1X3sbZ93z2qtv+5j+yA0MwAYZgAgzBBBiCCTAEW61Cv/XN4jlb/auq8fZVNVfuvFJl3as5T9bk+67ac+HfvB0YggkwBBNgCCbAEEyAIdh+Ffptr5I2WdnbW6uqt7bqk/Z1/+5V16skdjUf/5u3A0MwAYZgAgzBBBiCCTAEq6xC32ay5rzyb6pqj1Wfa7Jn+LZTbG97nm12YAgmwBBMgCGYAEMwAYZgqVXoyb7iqmptVV16cmJJVU2+z9n+7ePswBBMgCGYAEMwAYZgAgzBKqvQk7W+qonBZ/uKJ2eGrKw+eVXVVOq+b3nF8fq2HRiCCTAEE2AIJsAQTIAh2H4V+mwH7NttFcuzz7N3n77zCm9TNYP6ODswBBNgCCbAEEyAIZgAQ7DVKvTxns//OFubXfk3Z+u3k9/X2fMKb5u/PcwODMEEGIIJMAQTYAgmwBBstQq9NwFj7z5Vd666qq+vuMpkj+793dGT77nvsy/WwO3AEEyAIZgAQzABhmACDMG+Fotdw7W1v66+V/07WydfMTnTuK+mOvnMe/p+oVhZq+pv9bEDQzQBhmACDMEEGIIJMARbrUJ/uPL6qu9tMzHur+SvXLXyPGcnYL/ddudCdmAIJsAQTIAhmABDMAGGYJVV6Naez4MunMOwYbLLemX1FRf2Hv9VX93+IzswBBNgCCbAEEyAIZgAQ7DKudBvfd22e/bWmuz1PVvbn6wVVzl7LmRfh/kiOzAEE2AIJsAQTIAhmABDsPNzoRPnZkzWePtW76ucv03+jlDl7OQTvdDw8wkwBBNgCCbAEEyAIdhqL/SksxOnq9bq66nuM7l6X1/6ituq69v3sQNDMAGGYAIMwQQYggkwBNufC7253vWdxm+Tlc+3qh7myVkfidObQ89qtANDMAGGYAIMwQQYggkwBOvthe6rRvbd563vee7/7HtV39sq1VUnM05O9l5kB4ZgAgzBBBiCCTAEE2AIVnk64dnpBGfnb+z9myp7lc+zZyz2TUvee/OTv5is3HmRHRiCCTAEE2AIJsAQTIAh2H4vdFWvb1V9++yEhxV9a/V19r5V9RVP3qdv0sjZOd6PHRiiCTAEE2AIJsAQTIAh2GoV+mwduG+iQt8ZeZO9vlW10Kr3vOLsWY19NfC9tcyFht9IgCGYAEMwAYZgAgzBVk8nPFtlrdI3r3hF35mGk6cKrtx5ssa78jx7a93fm/3YgSGaAEMwAYZgAgzBBBiCrVahP1w5WJdeuc/b/TMWJt/Y3p1v69a+7Tut6ug2Fxp+IwGGYAIMwQQYggkwBKucC73n7KmCx6uIJasn6jsFcvKN7f0lmMgBPI8AQzQBhmACDMEEGIJV9kLvub8Xus/PeIdVd/4Z8zeqqELDzyfAEEyAIZgAQzABhmCrvdC3nea2p+rMvhVV04DvP2fwra/CPPkN9vU5F7IDQzABhmACDMEEGIIJMATbn8jR10l7tl5addXepIiz3dF9lfO+ORV9tf3J0wnNhYbfSIAhmABDMAGGYAIMwSrnQk/2qe7duW8W8dtkP3DVO+yrzVat/lb1fs7W/7fZgSGYAEMwAYZgAgzBBBiCrc6F7pvD3HefFX1dzVXPMzlbY3Iy88rqZ7+dsydOmgsNP58AQzABhmACDMEEGILtV6H7as59VbvJMw3P1q5XDM+OKFl9xdlfMfqmiHxkB4ZgAgzBBBiCCTAEE2AItlqF3rz7YBXxtkr13p331rrtmffc9lb7mAsNPI8AQzQBhmACDMEEGIKtzoWerKBWzZfuOzVvb/W+juVJZ09mrDrl8K1vHshb4TdoB4ZgAgzBBBiCCTAEE2AItt8LPTlR4eypeW+T/beTXd9n3/xt9fa3qic0kQN4HgGGaAIMwQQYggkwBKs8nTBRVd31tmkbZ2vXKxLPl5x8P4vswBBMgCGYAEMwAYZgAgzBVidyTNY532476a9qrbM93n3vsG8+ycpaVVf1vXm90MDzCDBEE2AIJsAQTIAh2GoVes/kjI7J6vGeqinZ98+FXlH1K8bZ7/34t2MHhmACDMEEGIIJMAQTYAi2P5HjthnCe2udncOQOFu7r+7aN3ukaq09rVPE7cAQTIAhmABDMAGGYAIMwXp7od8m665V+jpybzuHcXLSyNttU5erPkXf5JPHDgzRBBiCCTAEE2AIJsAQbLUX+sOV19cDV5yd59Ban9xY/W1y+nfV6iv65pwPn/loB4ZgAgzBBBiCCTAEE2AINt0L/dbX/bty574pEGc7n89OwF7Rd5pk1ffet1bhWY12YAgmwBBMgCGYAEMwAYZglb3QK/pqqrfdeWWtyW7kyR7dyVnNZ8+pPP7rgx0YggkwBBNgCCbAEEyAIdj+6YQrqirDfVMg+qqRK2v11TD3nD1RceV57u/xnpxY8tiBIZoAQzABhmACDMEEGIKtTuTom/k8udZtcxhW3DaxpK+z932fyc9edZ/JeemPHRiiCTAEE2AIJsAQTIAh2GoV+mwP6t4c3apZDX3V2pXnOVsDP+vs2ZER79AODMEEGIIJMAQTYAgmwBBs/3TCsx3LfRXCqikZt32KFVV18pU77z3P3uqTdx5e3Q4MwQQYggkwBBNgCCbAEGy/Cv02OTt6z1717+xpgCvuP2uvb63ECeGF867twBBMgCGYAEMwAYZgAgzBKqvQk/YqjVU9zMOVxo3VJ9fau+psJb/vPsPnS9qBIZgAQzABhmACDMEEGIKlVqGrnK189tU5qyqfe2utmHzClatWnP1l4SM7MAQTYAgmwBBMgCGYAEOwyir0/bM1qmqqfauvuH+yx+Sd91bvO6eyqk/eXGj4+QQYggkwBBNgCCbAEGy/Cn22rlilai7E/W/jts+1t9bkHI+qJ2xlB4ZgAgzBBBiCCTAEE2AI9jXZwAzUsgNDMAGGYAIMwQQYggkwBBNgCCbAEEyAIZgAQzABhmACDMEEGIL9CxSiob0a/XIGAAAAAElFTkSuQmCC

// Image Output
echo '<img src="' . $QrCodeDataUri . '" alt="OTP Configuration QrCode" />';
```
### Example QR Code image
![OTP Configuration QrCode](https://bitbucket.org/John_Sear/otp/raw/master/doc/otp-qrcode.png)
