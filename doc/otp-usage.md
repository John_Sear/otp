# OTP Code Usage

## OTP Code Configuration 
It is recommendet, but not needed to set up the [OTP Code Configuration](https://bitbucket.org/John_Sear/otp/src/master/doc/otp-configuration.md) DataTransferObject.

> If you use the default configuration, be sure to use **your own Base32 encoded Secret** in every usage, for working results!

## Generate and Validate OTP Code

### Example including OTP Code Configuration
```php
use JohnSear\OTP;

require __DIR__ . '/vendor/autoload.php';

$OTPCodeGenerator = new OTP\CodeGenerator();
$OTPCodeValidator = new OTP\CodeValidator($OTPCodeGenerator);
$OTPCodeProvider  = new OTP\CodeProvider($OTPCodeGenerator, $OTPCodeValidator);

$myOTPCodeConfiguration = (new OTP\CodeConfiguration())
    ->setDigits(6)
    ->setPeriod(15)
    ->setAlgorithm('SHA1')
    ->setOtpAuth('TOTP')
    ->setBase32Secret('BASE32ENCODEDSECRET')
    ->setIssuer('John_Sear | OTP Library')
    ->setUsername('John_Sear@gmx.de');

$OTPCodeProvider->setCodeConfiguration($myOTPCodeConfiguration);

// Generate
$OTPCode = $OTPCodeProvider->generateOTP(); // i.e. 123456

// Validate
$givenOTPCode = '654321';
$validated = $OTPCodeProvider->verifyOTP($givenOTPCode);

if($validated) {
    echo 'validated';
} else {
    echo 'not validated';
}
```

### Example only with own Base32 encoded secret
```php
use JohnSear\OTP;

require __DIR__ . '/vendor/autoload.php';

$OTPCodeGenerator = new OTP\CodeGenerator();
$OTPCodeValidator = new OTP\CodeValidator($OTPCodeGenerator);
$OTPCodeProvider  = new OTP\CodeProvider($OTPCodeGenerator, $OTPCodeValidator);

$ownBase32Secret = 'BASE32ENCODEDSECRET';

// Generate
$OTPCode = $OTPCodeProvider->generateOTP($ownBase32Secret); // i.e. 123456

// Validate
$givenOTPCode = '654321';
$validated = $OTPCodeProvider->verifyOTP($givenOTPCode, $ownBase32Secret);

if($validated) {
    echo 'validated';
} else {
    echo 'not validated';
}
```

## Generate QrCode Data Uri

### Example including OTP Code Configuration
```php
use JohnSear\OTP;

require __DIR__ . '/vendor/autoload.php';

$OTPCodeGenerator = new OTP\CodeGenerator();
$OTPCodeValidator = new OTP\CodeValidator($OTPCodeGenerator);
$OTPCodeProvider  = new OTP\CodeProvider($OTPCodeGenerator, $OTPCodeValidator);

$myOTPCodeConfiguration = (new OTP\CodeConfiguration())
    ->setDigits(6)
    ->setPeriod(15)
    ->setAlgorithm('SHA1')
    ->setOtpAuth('TOTP')
    ->setBase32Secret('BASE32ENCODEDSECRET')
    ->setIssuer('John_Sear | OTP Library')
    ->setUsername('John_Sear@gmx.de');

$OTPCodeProvider->setCodeConfiguration($myOTPCodeConfiguration);

// Generate QrCode Data Uri
$QrCodeDataUri = $OTPCodeProvider->generateQrCode();
// i.e. 123456

// Validate
$givenOTPCode = '654321';
$validated = $OTPCodeProvider->verifyOTP($givenOTPCode);

if($validated) {
    echo 'validated';
} else {
    echo 'not validated';
}
```

### Example only with own Base32 encoded secret
```php
use JohnSear\OTP;

require __DIR__ . '/vendor/autoload.php';

$OTPCodeGenerator = new OTP\CodeGenerator();
$OTPCodeValidator = new OTP\CodeValidator($OTPCodeGenerator);
$OTPCodeProvider  = new OTP\CodeProvider($OTPCodeGenerator, $OTPCodeValidator);

$ownBase32Secret = 'BASE32ENCODEDSECRET';

// Generate
$OTPCode = $OTPCodeProvider->generateOTP($ownBase32Secret); // i.e. 123456

// Validate
$givenOTPCode = '654321';
$validated = $OTPCodeProvider->verifyOTP($givenOTPCode, $ownBase32Secret);

if($validated) {
    echo 'validated';
} else {
    echo 'not validated';
}
``` 
